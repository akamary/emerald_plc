#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <assert.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <unistd.h>

#include <hexdump.h>
#include <data.h>

#define TIMEOUT 10 * 1000
#define BUF_SIZE 1024

void error(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void reply(int sock, char *buffer, size_t len);

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno = 4002;
    socklen_t clilen;
    char buffer[BUF_SIZE];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    static const int kOne = 1;
    int err = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &kOne, sizeof(kOne));
    assert(err == 0);
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");

    while (true)
    {
        listen(sockfd, 5);
        clilen = sizeof(cli_addr);
        std::cout << "ready to accept new connections." << std::endl;
        newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
        if (newsockfd < 0)
            error("accept()");
        std::cout << "client address : [" << inet_ntoa(cli_addr.sin_addr) << "]" << std::endl;
        bzero(buffer, BUF_SIZE);

        struct pollfd fds[1];
        fds[0].fd = newsockfd;
        fds[0].events = POLLIN;
        int ret = 0;

        while (true)
        {
            ret = poll(fds, 1, TIMEOUT);
            if (ret == -1)
            {
                error("poll()");
                break;
            }
            else if (ret == 0)
            {
                printf("%d seconds elapsed.\n", TIMEOUT / 1000);
                continue;
            }

            if (fds[0].revents & POLLHUP || fds[1].revents & POLLERR)
            {
                close(newsockfd);
                printf("poll error.");
                break;
            }

            if (fds[0].revents & POLLIN)
            {
                n = ::read(newsockfd, buffer, BUF_SIZE);
                if (n < 0)
                    error("reading from socket");
                printf("read %d bytes\n", n);
                if (n == 0)
                {
                    close(newsockfd);
                    printf("socket closed\n");
                    break;
                }
                else
                {
                    std::cout << hexdump(buffer, n) << std::endl;
                    int8_t checksum = 0;
                    for (uint16_t indx = 3; indx < (n - 1); indx++)
                    {
                        checksum += buffer[indx];
                    }

                    checksum = -checksum;
                    std::cout << ((checksum == buffer[n - 1]) ? "TRUE" : "FALSE") << std::endl;
                    if (checksum == buffer[n - 1])
                        reply(newsockfd, buffer, n);
                }
                fds[0].revents = 0;
            }
        }
    }

    ::close(newsockfd);
    ::close(sockfd);
    return 0;
}

void reply(int sock, char* buffer, size_t len)
{
    std::uint32_t *rwaddr = reinterpret_cast<std::uint32_t *>(buffer + HEADER_LENGTH);
    std::uint16_t *rwcount = reinterpret_cast<std::uint16_t *>(buffer + HEADER_LENGTH + sizeof(std::uint32_t));
    std::uint32_t addr = byteswap<std::uint32_t>(*rwaddr);
    std::uint16_t count = byteswap<std::uint16_t>(*rwcount);
    printf("requested 0x%X address\n", addr);
    printf("requested %u bytes\n", count);
    std::vector<char> ret(HEADER_LENGTH, 0x00);
    ret[0] = response_en::ACK;
    ret[1] = 'E';
    ret[2] = 'S';
    ret[3] = 'C';
    ret[4] = buffer[3];
    ret[5] = buffer[4];

    for (unsigned i = 0; i < count; i++)
    {
        uint8_t val = (i % 4 + 1) + 16 * (i % 4 + 1);
        ret.push_back(val);
    }
    int8_t checksum = 0;
    for (uint16_t indx = 4; indx < ret.size(); indx++)
    {
        checksum += ret[indx];
    }

    checksum = -checksum;
    ret.push_back(checksum);
    int n = ::send(sock, &ret[0], ret.size(), 0);
    printf("wrote %d bytes\n", n);
    std::cout << hexdump(&ret[0], ret.size()) << std::endl;
}