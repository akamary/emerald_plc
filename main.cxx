#include <iostream>
#include <memory>
#include <string>
#include <cstring>
#include <cerrno>

#include <errno.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>

#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <data.h>
#include <hexdump.h>


#define POLL_TIMEOUT 25000

struct sockaddr_in plc_sockaddr(const char *hostname, int port)
{
    struct sockaddr_in ipa;
    ipa.sin_family = AF_INET;
    ipa.sin_port = htons(port);

    struct hostent *localhost = gethostbyname(hostname);
    if (!localhost)
    {
        std::cerr << "resolveing : " << hostname << std::endl;
        return ipa;
    }

    char *addr = localhost->h_addr_list[0];
    //std::cout << "ip addr : " << addr << std::endl;

    int len = std::strlen(addr);
    std::memcpy(&ipa.sin_addr.s_addr, addr, len);

    return ipa;
}

void plc_print_ip(std::string host)
{
    struct hostent *he;
    struct in_addr a;
    he = gethostbyname(host.c_str());
    if (he)
    {
        std::cout << "name: " << he->h_name << std::endl;
        while (*he->h_aliases)
        {
            std::cout << "alias: " << *he->h_aliases++ << std::endl;
        }
        while (*he->h_addr_list)
        {
            bcopy(*he->h_addr_list++, (char *)&a, sizeof(a));
            std::cout << "address : " << inet_ntoa(a) << std::endl;
        }
    }
    else std::cout << "error";
}

int plc_connect(std::string ip, unsigned short port)
{
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (sock == -1)
    {
        std::cerr << "socket() failed" << std::endl;
        return -1;
    }

    int status = fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);
    if (status == -1)
    {
        std::cerr << "fcntl() failed" << std::endl;
        return -1;
    }

    struct sockaddr_in addr = plc_sockaddr(ip.c_str(), port);
    if (connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1)
    {
        if ((errno != EWOULDBLOCK) && (errno != EINPROGRESS))
        {
            std::cerr << "connect() failed" << std::endl;
            return -1;
        }
    }

    struct pollfd pfds[] = { {sock, POLLOUT, 0 } };
    int rc = poll(pfds, 1, POLL_TIMEOUT);
    if (rc > 0)
    {
        int error = -1; socklen_t len = sizeof(error);
        int retval = getsockopt(sock, SOL_SOCKET, SO_ERROR, &error, &len);
        if (retval == 0) errno = error;
        if (error != 0)
        {
            std::cerr << "connection failed" << std::endl;
            close(sock);
            return -1;
        }
        std::cout << "connected to [" << ip << "] on port [" << port << "]" << std::endl;
    }
    else if (rc == 0)
    {
        std::cerr << "connection timeout" << std::endl;
        close(sock);
        return -1;
    }

    return sock;
}

uint8_t disconnect_msg[] = {0x45,0x53,0x43,0x05,0x01,0x00,0x00,0x0F,0x00,0x00,0x00,0x03,0xE8}; 
uint8_t connect_msg[]    = {0x45,0x53,0x43,0x05,0x01,0x00,0x00,0x0F,0x00,0x00,0x00,0x14,0xD7};

int main()
{

    std::cout << "sample [" << sizeof(connect_msg) << "] : " << hexdump(connect_msg, sizeof(connect_msg)) << std::endl;

    emerald_plc_packet pkt;
    std::memcpy(&pkt[0], connect_msg, sizeof(connect_msg));
    pkt.set_total_len(sizeof(connect_msg));
    pkt.set_checksum(0);
    std::cout << "before checksum : " << hexdump(&pkt[0], pkt.get_total_len()) << std::endl;
    pkt.do_checksum();
    std::cout << "after checksum : " << hexdump(&pkt[0], pkt.get_total_len()) << std::endl;

    std::cout << "checksum : " << std::hex << (unsigned)pkt.get_checksum() << std::dec << " - " << (int)pkt.get_checksum() << std::endl;
    std::cout << "body length : " << pkt.get_body_len() << std::endl;

    pkt.read_long(0xBC002848);
    pkt.do_checksum();
    std::cout << "read long : " << std::endl << hexdump(&pkt[0], pkt.get_total_len()) << std::endl;
    std::cout << "body length : " << pkt.get_body_len() << std::endl;


    //return EXIT_SUCCESS;

    int sfd = plc_connect("10.245.153.194", 4002);
    //plc_print_ip("10.245.153.194");
    //plc_print_ip("cnn.com");
    //int sfd = plc_connect("10.242.171.131", 4002);

    if (sfd < 0)
    {
        std::cerr << "connection failed." << std::endl;
        return EXIT_FAILURE;
    }

    ssize_t rc = ::write(sfd, pkt.get_buffer(), pkt.get_total_len());

    std::cout << "wrote [" << rc << "] bytes" << std::endl;

    char buffer[1024];
    std::memset(buffer, 0x00, 1024);

    struct pollfd fd = {
        .fd = sfd,
        .events = POLLIN
    };

    rc = poll(&fd, 1, POLL_TIMEOUT);
    if ( rc > 0 && fd.revents & POLLIN )
    {
        rc = read(sfd, buffer, 1024);
    }
    else if (rc == 0)
    {
        std::cout << "poll() timeout" << std::endl;
    }
    else if (rc < 0)
    {
        std::cout << "poll() error : " << std::strerror(errno) << std::endl;
    }

    if (rc > 0)
    {
        std::cout << "read [" << rc << "] bytes" << std::endl;
        std::cout << hexdump(buffer, rc) << std::endl;
        auto vec = parse_values <std::uint32_t> (buffer, rc, 1);
        for (auto e : vec)
        {
            std::cout << "val : " << e << std::endl;
        }
    }

    close(sfd);

    std::cout << "closed." << std::endl;

    return EXIT_SUCCESS;
}