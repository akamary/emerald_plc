all:
	$(CXX) main.cxx -o main -Wall -std=c++17 -I. -g
	$(CXX) emplc_server.cxx -o emplc_server -std=c++20 -g -I.

clean:
	rm -rf emplc_server main