#include <iostream>
#include <cstring>

class test
{


public:
    test(/* args */);
    ~test();
    char str[100];

    const char* get_buff() const {return reinterpret_cast<const char*>(str);}
};

test::test(/* args */)
{
}

test::~test()
{
}

int main()
{
    const char hi[] = "hello world!";
    test t1;
    std::memcpy(t1.str, hi, sizeof(hi));
    test t2;
    t2 = t1;
    std::cout << t1.get_buff() << std::endl;
    std::cout << t2.get_buff() << std::endl;
}