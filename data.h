#pragma once

#include <cstdint>
#include <climits>
#include <vector>

#define MAX_BODY_LENGTH   496
#define MAX_FRAME_LENGTH  (496+13)
#define HEADER_LENGTH     12

enum op_code_en : std::uint16_t {
  RDBLKDATA       = 0x0100,    // Read Block Data 
  WRBLKDATA       = 0x0200,    // Write Block Data
  RQESCSTAT       = 0x0300,    // Request ESC Status
  SETFLAG         = 0x0400,    // Set Flag
  CLRFLAG         = 0x0500,    // Clear Flag
  RDFLAG          = 0x0600,    // Read Flag
  RDFLAGGROUP     = 0x0700,    // Read Flag Group
  AUTOTUNE        = 0x0800,    // AutoTune Device
  JOG             = 0x0900,    // Jog
  INDEX           = 0x0A00,    // Index
  STOPMOTION      = 11,   // Stop Motion
  WRIDN           = 12,   // Write IDN
  RDIDN           = 13,   // Read IDN
  RDDEVINFO       = 14,   // Read Device Information
  STOPPRG         = 15,   // Stop Program
  RESETPRG        = 16,   // Reset Program
  STARTPRG        = 17,   // Start Program
  SETAUTOSTART    = 18,   // Set AutoStart
  CLRAUTOSTART    = 19,   // Clear AutoStart
  RDPRGINFO       = 20,   // Read Program Information
  RDCNTRLINFO     = 21,   // Read Controller Info.
  DNET_STATUS     = 22,   // DeviceNet Status
  O_SCOPE         = 23,   // O Scope
  LOOP_UP         = 24,   // Bring Loop Up
  LOOP_DOWN       = 25,   // Bring Loop Down
  RDWATCHDATA     = 26,   // Read Watch Data
  SET_FLAGA       = 27,   // Set Flag Addressable
  CLR_FLAGA       = 28,   // Clear Flag Addressable
  RD_FLAGA        = 29,   // Read Flag Addressable
  RD_FLAGA_GROUP  = 30,   // Read Flag Group Addressable
  RDEVENTINFO     = 31,   // Read Event Information
  RDNODESSTAT     = 32,   // Read Nodes Status
  RDERRORLOG      = 35,   // Read Error Log
  RDFIXEDMEM      = 36,   // Clear Fixed Memory
};

enum controller_type_en : std::uint8_t {
  EMC2000     = 1,
  IMAX        = 2,
  EMC1000	    = 2,
  EMC2005	    = 3,
  EMAX        = 4,
  EMC2100S2   = 5,
  EMC2100S3   = 6,
  LMC400      = 7,
  LMAX        = 8,
  LMC400B     = 9,	
};

enum level_en : std::uint8_t {
  EDE         = 0x0F,
};


enum direction_en : std::uint8_t {
  SEND_TO_PLC = 0,
  FROM_PLC = 1,
};

enum response_en : std::uint8_t {
  ACK = 0x06,
  NACK_ADDR_RANGE = 0x10,	/* request of data outside available addresses */
  NACK_LEN_RECEIVE = 0x11,	/* command sent to EMERALD too long for buffer */
  NACK_BAD_TYPE = 0x12,	/* flag or data type does not exist */
  NACK_LEN_REQUEST = 0x13,	/* too much data requested from EMERALD */
  NACK_READ_ONLY = 0x14,	/* tried to write to read only data/flag */
  NACK_TIMEOUT = 0x16,	/* lost communications with EMERALD */
};

template <typename T>
T byteswap(T u)
{
    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        T u;
        unsigned char u8[sizeof(T)];
    } source, dest;

    source.u = u;

    for (size_t k = 0; k < sizeof(T); k++)
        dest.u8[k] = source.u8[sizeof(T) - k - 1];

    return dest.u;
}

//  Command Packet Header Values are 12 byte values layed out as follows:
//
//   0 0 0 0 0 0 0 0 0 0 1 1 
//   0 1 2 3 4 5 6 7 8 9 0 1 
//  +-----+-+-------+---+---+
//  |E S C|T|I|r r L|len|cmd|
//  +-----+-+-------+---+---+
//
//  where
//
//      ESC - is the command packet prefix 'E','S','C'
//
//      T - is the Controller type [1,8]
//
//      I - is the controller ID, Identifies a controller
//          on the multidrop RS485 interface [1,255]
//
//      rr - is reserved for future use
//
//      L - is the Level (0x0f indicates talker is EDE)
//
//      len - is the length of the command packet (in bytes) not
//            including the packet header or the trailing checksum
//
//      cmd - is the command OpCode
struct emerald_plc_packet
{
  emerald_plc_packet()
  : signature(frame)
  , cnt_type(reinterpret_cast<controller_type_en*> (frame + 3))
  , id(reinterpret_cast<std::uint8_t*> (frame + 4))
  , reserved(reinterpret_cast<std::uint16_t*>(frame + 5))
  , level(reinterpret_cast<level_en*>(frame + 7))
  , len(reinterpret_cast<std::uint16_t*>(frame + 8))
  , cmd(reinterpret_cast<op_code_en*>(frame + 10))
  , checksum(reinterpret_cast<std::int8_t*> (frame + HEADER_LENGTH))
  {
    std::memset(frame, 0x00, MAX_FRAME_LENGTH);
    std::memcpy(signature, "ESC", 3);
    *cnt_type     = EMC2100S2;
    *id           = 0x01;
    *level        = EDE;
    *reserved     = 0x0000;
    total_len     = HEADER_LENGTH + 1;
  }

private:

  char                  frame[MAX_FRAME_LENGTH];
  char*                 signature;
  controller_type_en*   cnt_type;     // controller type
  std::uint8_t*         id;
  std::uint16_t*        reserved;
  level_en*             level;
  std::uint16_t*        len;          // Packet Header (12 bytes) and Packet Chechsum (1 byte) not included
  op_code_en*           cmd;
  std::int8_t*          checksum;

  uint16_t              total_len;

public:

  void set_total_len(std::uint32_t tlen)
  {
      if (tlen < 13) throw std::runtime_error("total length error");
      total_len = tlen;
      *len = byteswap <std::uint16_t> (total_len - 13);
  }

  std::uint32_t get_total_len()
  {
      return total_len;
  }

  void set_opcode(op_code_en op)
  {
      *cmd = op;
  }

  void do_checksum()
  {
      int8_t checksum = 0;
      for (uint16_t indx = 3; indx < (total_len - 1); indx++)
      {
        checksum += frame[indx];
      }
      this->checksum = reinterpret_cast<std::int8_t*>(frame + total_len - 1);
      *this->checksum = (-checksum);
  }

  void set_checksum(std::int8_t val)
  {
      *this->checksum = val;
  }

  int8_t get_checksum()
  {
      return *this->checksum;
  }

  uint16_t get_body_len()
  {
    return byteswap <std::uint16_t> (*this->len);
  }

  char& operator[](size_t indx)
  {
    return frame[indx];
  }

  char operator[](size_t indx) const
  {
    return  frame[indx];
  }

  const char* get_buffer()
  {
    return reinterpret_cast<const char*>(frame);
  }

  /**
   * read_short reads a single or an array of 16-bit integers
   * @param addr stating memory address
   * @param num number of array elements (default is one)
  */
  void read_short(std::uint32_t addr, unsigned num = 1)
  {
    *cmd = RDBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint16_t) * num);
    set_total_len(HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t) + 1);
  }

  /**
   * read_long reads a single or an array of 32-bit integers
   * @param addr stating memory address
   * @param num number of array elements (default is one)
  */
  void read_long(std::uint32_t addr, unsigned num = 1)
  {
    *cmd = RDBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint32_t) * num);
    set_total_len(HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t) + 1);
  }

  void write_short(std::uint32_t addr, std::uint16_t value)
  {
    *cmd = WRBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    std::uint16_t* data = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint16_t));
    *data    = byteswap <std::uint16_t> (value);
    set_total_len(HEADER_LENGTH + sizeof(std::uint32_t) + 2 * sizeof(std::uint16_t) + 1);
  }

  void write_long(std::uint32_t addr, std::uint32_t value)
  {
    *cmd = WRBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    std::uint32_t* data = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t));
    *rwaddr  = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint32_t));
    *data    = byteswap <std::uint32_t> (value);
    set_total_len(HEADER_LENGTH + 2 * sizeof(std::uint32_t) + sizeof(std::uint16_t) + 1);
  }

  void read_float(std::uint32_t addr, unsigned num = 1)
  {
    *cmd = RDBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(float) * num);
  }

  void read_double(std::uint32_t addr, unsigned num = 1)
  {
    *cmd = RDBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(double) * num);
  }

  void write_double(std::uint32_t addr, double value)
  {
    *cmd = WRBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    double* data = reinterpret_cast<double*>(frame + HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t));
    *rwaddr = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint16_t));
    *data    = byteswap <double> (value);
    set_total_len(HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t) + sizeof(double) + 1);
  }

  void write_float(std::uint32_t addr, float value)
  {
    *cmd = WRBLKDATA;
    std::uint32_t* rwaddr = reinterpret_cast<std::uint32_t*>(frame + HEADER_LENGTH);
    std::uint16_t* rwcount = reinterpret_cast<std::uint16_t*>(frame + HEADER_LENGTH + sizeof(std::uint32_t));
    float* data = reinterpret_cast<float*>(frame + HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t));
    *rwaddr  = byteswap <std::uint32_t> (addr);
    *rwcount = byteswap <std::uint16_t> (sizeof(std::uint32_t));
    *data    = byteswap <float> (value);
    set_total_len(HEADER_LENGTH + sizeof(std::uint32_t) + sizeof(std::uint16_t) + sizeof(float) + 1);
  }

};

  template <typename T>
  std::vector<T> parse_values(char* buffer, size_t len, std::uint16_t num)
  {
    std::vector<T> ret(num);
    if (buffer[0] != ACK) throw std::runtime_error("NACK");
  
    int8_t checksum = 0;
    for (uint16_t indx = 4; indx < (len - 1); indx++)
    {
      checksum += buffer[indx];
    }

    checksum = -checksum;

    std::cout << std::hex << (unsigned)checksum << " - " << (unsigned)buffer[len - 1] << std::dec << std::endl;
    std::cout << std::hex << (checksum == buffer[len - 1]) << std::dec << std::endl;

    T* array;
    array = reinterpret_cast<T*>(buffer + HEADER_LENGTH + 1);
    for (std::uint16_t indx = 0; indx < num; indx++)
    {
      ret[indx] = byteswap<T>(array[indx]);
    }

    return ret;
  }
